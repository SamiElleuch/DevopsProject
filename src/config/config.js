module.exports = {
  db: {
    dbHostName: "localhost",
    dbPort: 27017,
    dbName: "HelloWorld",
  },
  hostname: process.env.HOST || "localhost",
  port: process.env.PORT || 8080,
  secretKey: "_Fill_ME_",
};
