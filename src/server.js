const express = require("express");

 const helmet = require("helmet");

 const cors = require("cors");

 const logger = require("morgan");

 const mongoose = require("mongoose");

 const passport = require("passport");

const app = express();

const promBundle = require("express-prom-bundle");

// Add the options to the prometheus middleware most option are for http_request_duration_seconds histogram metric
const metricsMiddleware = promBundle({
  includeMethod: true, 
  includePath: true, 
  includeStatusCode: true, 
  includeUp: true,
  customLabels: {project_name: 'hello_world', project_type: 'test_metrics_labels'},
  promClient: {
      collectDefaultMetrics: {
      }
    }
});
// add the prometheus middleware to all routes
app.use(metricsMiddleware)

const server = require("http").Server(app);

/*****Middlewares*****/

 app.use(helmet());
 app.use(logger("dev"));
 app.use(cors());
 app.use(passport.initialize());
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

/******************************/

//imports
//auto require our routes
//BY DEFAULT routes are on  localhost/api/v1/{routefilename}
require("./routers/router")(app);
const config = require("./config/config");

server.listen(config.port, config.hostname, () => {
  console.log(`Server running at http://${config.hostname}:${config.port}/`);
});

module.exports = app; // for testing
