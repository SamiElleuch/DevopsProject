const config = require("../config/config");
const fiboController = require("../controllers/fibo");
//During the test the env variable is set to test
process.env.NODE_ENV = 'test';
//Require the dev-dependencies
let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../server');
let should = chai.should();
let expect = chai.expect;


chai.use(chaiHttp);
//Our parent block
describe('Fibonnaci Sequence', () => {

/*
  * Test the /GET route
  */
  describe('/GET Fibo Sequence number', () => {
      it('it should return the fibonacci number in the corresponding sequence', (done) => {
        chai.request(server)
            .get('/api/v1/func/fibo/1')
            .end((err, res) => {
                  res.should.have.status(200);
                  result= JSON.parse(JSON.stringify(res.body))
                  expect(result).to.have.property('FiboNumberInSequence');
                  expect(result.FiboNumberInSequence).to.equal(1);
              done();
            });
      });
      it('it should return the fibonacci number in the corresponding sequence', (done) => {
        chai.request(server)
            .get('/api/v1/func/fibo/9')
            .end((err, res) => {
                  res.should.have.status(200);
                  result= JSON.parse(JSON.stringify(res.body))
                  expect(result).to.have.property('FiboNumberInSequence');
                  expect(result.FiboNumberInSequence).to.equal(34);
              done();
            });
      });it('it should return the fibonacci number in the corresponding sequence', (done) => {
        chai.request(server)
            .get('/api/v1/func/fibo/17')
            .end((err, res) => {
                  res.should.have.status(200);
                  result= JSON.parse(JSON.stringify(res.body))
                  expect(result).to.have.property('FiboNumberInSequence');
                  expect(result.FiboNumberInSequence).to.equal(1597);
              done();
            });
      });

  });

});
