
## Developed Using 
- Express-Booster bootstraping librairy  (node express) 
- Mocha and Chai for testing 
- Gitlab CI/CD with Helm and kubenetes for deployment
- Azure AKS for the kubernetes cluster


## Setup


- In order to install all packages follow the steps below:

```shell
$ cd DevopsProject 

```


> install npm and bower packages

```shell
$ npm install
```

> now run the backend

```shell
$ npm run start 👍
```
> now run the backend  in Debug mode with nodemon

```shell
$ npm run dev 👍
```

> to run tests with mocha and chai

```shell
$ npm run test 👍
```
### How To use

- To find the fibonnaci number in the sequence just pass the number as follows   </br>
 `fibo[16] <=> http://localhost:8081/api/v1/func/fibo/16`
